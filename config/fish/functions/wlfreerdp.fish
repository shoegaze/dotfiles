function wlfreerdp --description 'wlfreerdp alias'
 command wlfreerdp3 /w:1895 /h:1010 /gdi:hw +gfx-h264 +grab-keyboard +clipboard +fonts +aero -window-drag +menu-anims +themes +wallpaper +rfx +async-input -async-update +toggle-fullscreen $argv;
end
